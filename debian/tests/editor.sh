#! /bin/bash
set -uo pipefail

testEditor(){
    mkdir -p $AUTOPKGTEST_TMP/.urlwatch/lib
    echo -e "---\nname: \"tmp listing\"\ncommand: \"ls -al $AUTOPKGTEST_TMP\"" > $AUTOPKGTEST_TMP/.urlwatch/urls.yaml
    export HOME=$AUTOPKGTEST_TMP
    unset VISUAL
    unset EDITOR
    echo -e "#! /bin/bash\necho \"Running editor on \$1\"\necho '#' >> \$1" > $AUTOPKGTEST_TMP/editor
    chmod a+x $AUTOPKGTEST_TMP/editor
    export PATH=$AUTOPKGTEST_TMP:$PATH
    urlwatch --edit
    result=$?
    assertEquals "Urlwatch should use editor and not return 1" 0 $result
    lastChar=`tail -c 2 $AUTOPKGTEST_TMP/.urlwatch/urls.yaml`
    assertEquals "last char of edited file should be #" '#' $lastChar
}

# load shunit2
. shunit2
