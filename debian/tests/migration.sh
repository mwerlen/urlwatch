#! /bin/bash
set -uo pipefail

testMigration(){
    mkdir -p $AUTOPKGTEST_TMP/.urlwatch/lib
    echo -e "|ls -al $AUTOPKGTEST_TMP" > "$AUTOPKGTEST_TMP/.urlwatch/urls.txt"
    echo -e "from urlwatch import html2txt\ndef filter(url, data):\n        if url.startswith(\"http://urlwatch.com/\"):\n                return html2txt.html2text(data, method='html2text')" > "$AUTOPKGTEST_TMP/.urlwatch/lib/hooks.py"
    export HOME=$AUTOPKGTEST_TMP
    urlwatch
    result=$?
    assertEquals "Urlwatch should return an exit code 0" 0 $result
    [ -f "$AUTOPKGTEST_TMP/.urlwatch/urls.yaml" ] || fail "File urls.yaml should have been written"
    [ -f "$AUTOPKGTEST_TMP/.urlwatch/urlwatch.yaml" ] || fail "File urlwath.yaml should have been written"
}

# load shunit2
. shunit2
