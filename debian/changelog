urlwatch (2.29-1) unstable; urgency=medium

  * New upstream release
  * Update patches and remove merged ones
  * Update dependencies versions
  * Update Debian standards version
  * Simplify d/control

 -- Maxime Werlen <maxime@werlen.fr>  Thu, 31 Oct 2024 22:13:26 +0100

urlwatch (2.28-3) unstable; urgency=medium

  * Apply patch to switch from dead appdirs to platformdirs (Closes: #1068014)
  * Update copyright years

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 29 Mar 2024 21:56:51 +0100

urlwatch (2.28-2) unstable; urgency=medium

  * Backport upstream fix on LXML 5 (Closes: #1064736)

 -- Maxime Werlen <maxime@werlen.fr>  Tue, 27 Feb 2024 09:59:34 +0100

urlwatch (2.28-1) unstable; urgency=medium

  * New upstream release
  * Update patches
  * Update copyright years
  * Update required python version
  * Update standards
  * Remove old NEWS file, now unnecessary

 -- Maxime Werlen <maxime@werlen.fr>  Sat, 24 Jun 2023 14:03:37 +0200

urlwatch (2.25-1) unstable; urgency=medium

  * New upstream release
  * Update patches
  * Update copyright years
  * Update dependencies

 -- Maxime Werlen <maxime@werlen.fr>  Wed, 16 Mar 2022 22:39:27 +0100

urlwatch (2.24-1) unstable; urgency=medium

  * New upstream release
  * Update patches to new version
  * Add autopkgtest for python
  * Update pybuild setup to run all upstream tests
  * Removed Built-using following lintian alert built-using-field-on-arch-all-package

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 12 Nov 2021 16:15:01 +0100

urlwatch (2.23-2) unstable; urgency=medium

  * Updated Standards-Version to 4.6.0
  * Upload to unstable after Bullseye release
  * Fix build dependencies (lintian report)

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 03 Oct 2021 18:14:15 +0200

urlwatch (2.23-1) experimental; urgency=medium

  * New upstream release
  * Make lintian happy with control fields capitalization form
  * Update copyright years
  * Update patch offsets and disable one new test

 -- Maxime Werlen <maxime@werlen.fr>  Wed, 14 Apr 2021 18:34:30 +0200

urlwatch (2.22-1) unstable; urgency=medium

  * New upstream release
  * Update patches
  * Drop support for Python 3.5
  * Updated Standards-Version to 4.5.1

 -- Maxime Werlen <maxime@werlen.fr>  Sat, 19 Dec 2020 18:06:46 +0100

urlwatch (2.21-1) unstable; urgency=medium

  * New upstream release

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 31 Jul 2020 08:07:57 +0200

urlwatch (2.20-1) unstable; urgency=medium

  * New upstream release
  * Update patches
  * Handle upstream switch from nose to pytest

 -- Maxime Werlen <maxime@werlen.fr>  Thu, 30 Jul 2020 23:41:44 +0200

urlwatch (2.19-1) unstable; urgency=medium

  * New upstream release
  * Update patchs
  * Update build to allow new tests to pass
  * Update copyright years
  * Build and package HTML documentation from RST files
  * Git ignore debian/urlwatch.debhelper.log file

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 19 Jul 2020 18:24:02 +0200

urlwatch (2.18-2) unstable; urgency=medium

  * Bump to compat 13 and use debhelper-compat virtual package
  * Add an explicit Rules-Requires-Root field (value no)
  * Fix pycodestyle E741 violations (patch from upstream, Closes: Bug#963309)

 -- Maxime Werlen <maxime@werlen.fr>  Tue, 23 Jun 2020 21:56:17 +0200

urlwatch (2.18-1) unstable; urgency=medium

  * New upstream release
  * Updated Standards-Version to 4.5.0
  * Update Python minimum version to 3.5
  * Adjust patches to changed files
  * Removing Dockerfile from package
  * Add optional dependency on jsbeautifier
  * Fixed Build-Depends error

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 08 May 2020 14:57:38 +0200

urlwatch (2.17-1) unstable; urgency=medium

  * New upstream release
  * Handling .gitignore properly in d/.gitignore

 -- Maxime Werlen <maxime@werlen.fr>  Thu, 18 Apr 2019 15:01:55 +0200

urlwatch (2.16-1) unstable; urgency=medium

  * New upstream release
  * Updated patches
  * Updated Standards-Version to 4.3.0
  * Updated package description

 -- Maxime Werlen <maxime@werlen.fr>  Tue, 29 Jan 2019 22:19:26 +0100

urlwatch (2.15-2) unstable; urgency=medium

  * Patch python regex to avoid python 3.7 futureWarning (Closes: #914839)

 -- Maxime Werlen <maxime@werlen.fr>  Sat, 01 Dec 2018 16:37:42 +0100

urlwatch (2.15-1) unstable; urgency=medium

  * New upstream release
  * Upgrade d/watch version

 -- Maxime Werlen <maxime@werlen.fr>  Sat, 03 Nov 2018 14:00:11 +0100

urlwatch (2.14-1) unstable; urgency=medium

  * New upstream release
  * Updated patches
  * Updated Standards-Version to 4.2.1
  * Added new dependency python3-lxml
  * Added examples install

 -- Maxime Werlen <maxime@werlen.fr>  Sat, 01 Sep 2018 22:24:56 +0200

urlwatch (2.13-1) unstable; urgency=medium

  * New upstream release
  * Updated patches
  * Added wdiff as suggested dependency

 -- Maxime Werlen <maxime@werlen.fr>  Thu, 07 Jun 2018 21:09:23 +0200

urlwatch (2.11-1) unstable; urgency=medium

  * New upstream release
  * Updated patches to new upstream sources
  * Removed trailing spaces
  * Removed X-Python3-Version per Lintian rule ancient-python-version-field
  * Updated Standards-Version to 4.1.4

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 20 May 2018 15:26:59 +0200

urlwatch (2.9-1) unstable; urgency=medium

  * New upstream release
  * Removed patch included upstream (fixSetuptoolsWarning.diff)
  * Fixed lintian's tag default-mta-dependency-not-listed-first
  * Fixed some check-all-the-things warnings

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 25 Mar 2018 16:12:36 +0200

urlwatch (2.8-3) unstable; urgency=medium

  * Fix missing optional dependencies (Closes: #891884)

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 09 Mar 2018 22:03:58 +0100

urlwatch (2.8-2) unstable; urgency=medium

  * Changed dependency on keyring to recommends (Closes: #891456)
  * Fixed autopkgtests
  * Fixed typo in patch headers

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 02 Mar 2018 00:24:12 +0100

urlwatch (2.8-1) unstable; urgency=medium

  * New upstream release
  * Changed copyright year
  * Switch upstream site and mail to tph.io instead of tphinfo.com
  * Wrapped debian/watch
  * Removed patch included in upstream
  * Patched to fallback on editor (Closes: #890835)

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 25 Feb 2018 14:23:09 +0100

urlwatch (2.7-1) unstable; urgency=low

  * Upgrade Upstream version 2.7. (Closes: #798312)
  * Removed existing patches as changes wasn't pertinent anymore.
  * Added a patch to make nose tests work with pybuild (accepted by upstream).
  * Added a patch fo fix a setuptools warning (accepted by upstream).
  * Added a patch to remove dependencies from upstream Readme.
  * Added upstream metadata information.
  * Added a NEWS.debian file about migration from urlwatch 1.15
  * Added some tests.
  * Upgraded DH compat to 11.
  * Changed source repo to salsa.debian.org.
  * New maintainer. (Closes: #831272)
  * New version, now support ssl_ni_verify option. (Closes: #771907)

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 09 Feb 2018 06:21:33 +0100

urlwatch (1.15-4) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #831272)
  * Updated DH level to 10.
  * debian/control:
      - Added dh-python to Build-Depends field.
      - Bumped Standards-Version to 4.0.0.
      - Changed Priority from extra to optional.
      - Updated VCS fields.
  * debian/copyright: full updated.
  * debian/urlwatch.docs: renamed to debian/docs.
  * debian/watch:
      - Bumped version to 4.
      - Using GitHub as source.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 03 Aug 2017 11:28:20 -0300

urlwatch (1.15-3) unstable; urgency=low

  * Bumped up Standards-Version up to 3.9.4: (no changes).
  * Switched to X-Python-Version rather than XS-Python-Version in d.control.
  * Switched to dh_python2 (Closes: #689002) :
    - d.rules : Added --with python2 arg for debhelper
    - d.control : Removed BDI on python-support but added BDI against python
                  (>= 2.6.6-3~).
  * Fixed old URI as mentionned by lintian vcs-filed-not-canonical:
    - Replaced git.debian.org by anonscm.debian.org in d.control

 -- Franck Joncourt <franck@debian.org>  Wed, 17 Jul 2013 21:11:27 +0200

urlwatch (1.15-2) unstable; urgency=low

  * Added missing dependency in d.control against python-concurrent.futures
    (Closes: #688950).
  * Refreshed copyright holders in d.copyright.

 -- Franck Joncourt <franck@debian.org>  Thu, 27 Sep 2012 18:23:54 +0200

urlwatch (1.15-1) unstable; urgency=low

  * Imported Upstream version 1.15. (Closes: #647293)
    + Added BDI on python-concurrent.futures.
  * Updated years of copyrights in d.copyright.
  * Bumped up Standards-Version to 3.9.3:
    + Refreshed copyright holders and updated d.copyright to comply with the
      1.0 machine-readable copyright file specification.
  * Added patch to fix filter example in the manpage (Closes: #609312).

 -- Franck Joncourt <franck@debian.org>  Wed, 26 Sep 2012 21:29:55 +0200

urlwatch (1.11-1) unstable; urgency=low

  * New upstream release.
  * Bumped up Standards-Version to 3.9.1:
    + Do not refer to the BSD licence in /usr/share/common-licenses anymore.
      As a matter of fact the license was already included in d.copyright.
  * Removed useless BD on dpkg.
  * d.copyright:
    + Refreshed to follow the latest DEP5.
    + Updated copyright years for upstream.
  * Updated d.rules to use override targets:
    + Updated versionned dependency on debhelper fom 7 to 7.0.50.

 -- Franck Joncourt <franck@debian.org>  Fri, 30 Jul 2010 19:14:16 +0200

urlwatch (1.10-1) unstable; urgency=low

  * New upstream release:
    + The title of the manpage is now updated. Removed manpage.diff patch.
  * Bumped up Standards-Version to 3.8.4 (no change).

 -- Franck Joncourt <franck@debian.org>  Sun, 23 May 2010 20:19:08 +0200

urlwatch (1.9-2) unstable; urgency=low

  * Refreshed maintainer email address:
    + d.control: Updated email address and removed DM-Upload-Allowed flag.
    + d.copyright: Refreshed email address.
  * Upgraded to python2.6:
    + d.rules: Use of --install-layout=deb.
  * Switch to dpkg-source 3.0 (quilt) format.
    + Removed useless README.source which only documented the quilt usage.
    + Removed quilt framework (d.control, d.rules).
    + Added new BD against dpkg (>= 1.15.5.4) in d.control.

 -- Franck Joncourt <franck@debian.org>  Sun, 17 Jan 2010 14:22:21 +0100

urlwatch (1.9-1) unstable; urgency=low

  * New upstream Version.
  * Bumped up Standards-Version to 3.8.3 (no change).
  * Added patch manpage.diff to fix title of the manpage.

 -- Franck Joncourt <franck.mail@dthconnex.com>  Thu, 01 Oct 2009 08:35:16 +0200

urlwatch (1.8-1) unstable; urgency=low

  * New Upstream Version
  * Bumped up Standards-Version to 3.8.2 (no change).
  * Fixed crash due to httplib exceptions (BadStatusLine) not handled.
   (Closes: #529740)

 -- Franck Joncourt <franck.mail@dthconnex.com>  Mon, 10 Aug 2009 16:45:18 +0200

urlwatch (1.7-1) unstable; urgency=low

  * New Upstream Version

 -- Franck Joncourt <franck.mail@dthconnex.com>  Mon, 05 Jan 2009 15:45:20 +0100

urlwatch (1.5-1) unstable; urgency=low

  * Initial release (Closes: #505968)

 -- Franck Joncourt <franck.mail@dthconnex.com>  Sun, 07 Dec 2008 17:33:06 +0100
